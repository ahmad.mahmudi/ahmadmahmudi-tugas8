import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Modal,
} from 'react-native';
import React from 'react';
import {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from './url';
import {useIsFocused} from '@react-navigation/native';
import Axios from 'axios';

const Home = ({navigation, route}) => {
  const [modal, setModal] = useState(false);

  const [dataMobil, setDataMobil] = useState('');
  const inFocused = useIsFocused();

  const convertCurrency = (nominal = 0, currency) => {
    let rupiah = '';
    const nominalref = nominal.toString().split('').reverse().join('');
    for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
        rupiah += nominalref.substr(i, 3) + '.';
      }
    }

    if (currency) {
      return (
        currency +
        rupiah
          .split('', rupiah.length - 1)
          .reverse()
          .join('')
      );
    } else {
      return rupiah
        .split('', rupiah.length - 1)
        .reverse()
        .join('');
    }
  };

  const getDataMobil = async () => {
    Axios.get(`${BASE_URL}mobil`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('response', response);
        if (response.status === 200) {
          filterData(response.data.items);
        } else {
          if (response.status === 401 || response.status === 402) {
            return false;
          }
          if (response.status === 500) {
            return false;
          }
        }
      })
      .catch(error => {
        console.log('error', error);
      });
  };
  // filterData(result.items);

  const filterData = data => {
    let dataPush = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].title) {
        dataPush = [...dataPush, data[i]];
      }
    }
    console.log(dataPush);
    setDataMobil(dataPush);
  };

  useEffect(() => {
    getDataMobil();
  }, [inFocused]);

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Text
        style={{fontWeight: 'bold', fontSize: 20, margin: 15, color: '#000'}}>
        Home Screen
      </Text>
      <FlatList
        data={dataMobil}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('Add Data', item)}
            activeOpacity={0.8}
            style={{
              width: '90%',
              alignSelf: 'center',
              marginTop: 15,
              borderColor: '#dedede',
              borderWidth: 1,
              borderRadius: 6,
              padding: 12,
              flexDirection: 'row',
            }}>
            <View style={{width: '50%', paddingHorizontal: 10}}>
              <Image
                style={{width: '90%', height: 100, resizeMode: 'contain'}}
                source={{uri: item.unitImage}}
              />
            </View>
            <View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{fontSize: 14, fontWeight: 'bold', color: '#000'}}>
                  Nama Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {item?.title ?? 'No data'}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{fontSize: 14, fontWeight: 'bold', color: '#000'}}>
                  Total KM :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {item.totalKM}
                </Text>
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{fontSize: 14, fontWeight: 'bold', color: '#000'}}>
                  Harga Mobil :
                </Text>
                <Text style={{fontSize: 14, color: '#000'}}>
                  {convertCurrency(item.harga, 'Rp. ')}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
      />
      <Modal visible={false}></Modal>
      <TouchableOpacity
        onPress={() => navigation.navigate('Add Data')}
        style={{
          flex: 1,
          backgroundColor: 'red',
          width: 50,
          height: 50,
          position: 'absolute',
          bottom: 20,
          right: 20,
          borderRadius: 25,
        }}>
        <View
          style={{
            width: 40,
            height: 3,
            backgroundColor: 'white',
            position: 'absolute',
            top: 23,
            left: 5,
          }}
        />
        <View
          style={{
            width: 3,
            height: 40,
            backgroundColor: 'white',
            position: 'absolute',
            top: 4,
            left: 23,
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
